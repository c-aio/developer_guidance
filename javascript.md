# Doterb Code Style Guide - javascript

## A. Naming Convention

### A.1 General Rules:

- Gunakan bahasa inggris
- Gunakan nama yang representatif
- Gunakan waktu yang cukup untuk memikirkan nama yang representatif
  untuk semua code yang anda tulis, jangan tergesa - gesa, diskusi dengan
  team jika diperlukan.


### A.2 Aturan Penamaan File

- Gunakan huruf kecil
- Jka nama class terdiri dari 2 kata, maka gunakan ‘-’ ( minus char )

**Contoh:**

	user.js

	phone-number.js

### A.3 Aturan Penamaan Class

Setiap kata pertama menggunakan huruf besar ( Camelize )

**Contoh:**

	class User

    class PhoneNumber


### A.4 Aturan Penamaan Constant

- Gunakan huruf besar untuk constant
- Gunakan '_' untuk memisahkan kata

**Contoh:**

POLICE_PHONE_NUMBER   = '911'

POLICE_OFFICE_ADDRESS = 'Jalan Kantor Polisi no 1'


### A.5 Aturan penamaan Variable

- Gunakan penamaan dengan huruf kecil di awal kata pertama dan huruf besar
  di awal kata berikutnya tanpa tanda hubung ( Camelize convention )
- Perhatikan penggunaan kata singular dan plural dalam english. Misalnya 'people'
  untuk variable array and 'person' untuk single human data.
- Don't be so cool, define human readable name

**Contoh:**

Bad:

	fname   = 'John';
	w1      = 40;
	w2      = 50;
	total   = w1 + w2;
  	persons = [];
  	city    = ['Bandung', 'Tasikmalaya', 'Garut', 'Sumedang'];

Good:

	firstName    = 'John';
	firstWeight  = 0.1;
	secondWeight = 0.2;
	totalWeight  = firstWeight + secondWeight;
  	people       = A();
  	cities       = ['Bandung', 'Tasikmalaya', 'Garut', 'Sumedang'];


### A.6 Aturan penamaan method

- Gunakan Camelize convention
- Same as above, please use human readable name

Contoh:

	function fullName() {
      ...
	}

## B Line & Words Spacing Convention

Dalam javascripts community ada beberapa pihak yang membuat code styles indentation yang berbeda. Karena itu sangat penting untuk kita menentukan mana yang kita gunakan. Setelah mepertimbangkan beberapa hal dalam segi efisiensi, maka berikut ini yang kita pakai:


### B.1 Indentation
- Gunakan 2 spasi
- Jangan gunakan tab


### B.2 Assignments & Operations

- Buat codes assignment terlihat sejajar
- Kalau ada code yang terlalu panjang jika dibuat satu baris, maka buat menjadi beberapa baris
