# Doterb Code Style Guide

## Why ?

*Quality is not an act, it is a habit.*

(Aristotle)


*Quality is never an accident. It is always the result of intelligent effort*

(John Ruskin)


### Purpose

Code Styles ditentukan agar semua developers dapat menulis code berdasarkan standard / base yang sama. Bukan membatasi kreatifitas dalam interpretasi logika dalam sebuah algoritma, tapi memastikan bahwa code yang dibuat mengacu pada kerangka kerja yang sama.

Penerapan fungsi standarisasi dalam industri tech-art selain IT dapat dilihat misalnya pada
taman Sri Baduga di Kerawang. Di sekeliling tembok taman, pada setiap pilar ada ukiran tokoh pewayangan dengan berbagai bentuk. Walaupun ukiran - ukiran tersebut berbeda dan kemungkinan
dikerjakan oleh seniman yang berbeda juga, tapi secara umum ukiran tersebut selaras ketika dipajang berdampingan. Hal ini pastilan karena mereka punya design base yang sama, sehingga
hasil ukirannya selaras dalam hal skala objek dan nilai artistik.


### Goal

- Readable codes
- Efficient codes
- Maintenable and Reusable codes


### Available Guides

- [Ruby on Rails](https://bitbucket.org/c-aio/developer_guidance/src/master/ruby.md)
- [Javascript](https://bitbucket.org/c-aio/developer_guidance/src/master/javascript.md)
