# Doterb Code Style Guide - Ruby on Rails

Secara umum, kita mengikuti convention yang sudah ditentukan oleh [rubocop](https://github.com/rubocop-hq/ruby-style-guide.).

Code styles yang dibuat disini adalah convention untuk hal yang belum ditentukan
rubocop atau menjelaskan kembali rules yang sudah ada dengan contoh yang berbeda.

## A. Naming Convention

### A.1 General Rules:

- Gunakan bahasa inggris
- Gunakan nama yang representatif
- Gunakan waktu yang cukup untuk memikirkan nama yang representatif
  untuk semua code yang anda tulis, jangan tergesa - gesa, diskusi dengan
  team jika diperlukan.


### A.2 Aturan Penamaan File

- Gunakan huruf kecil
- Jka nama class terdiri dari 2 kata, maka gunakan ‘_’

**Contoh:**

	user.rb

	phone_number.rb


### A.3 Aturan Penamaan Class

Setiap huruf pertama dalam sebuah kata menggunakan huruf besar ( Camelize )

**Contoh:**

	class User

    class PhoneNumber


### A.4 Aturan Penamaan Constant

- Gunakan huruf besar untuk constant
- Gunakan '_' untuk memisahkan kata

**Contoh:**

POLICE_PHONE_NUMBER   = '911'

POLICE_OFFICE_ADDRESS = 'Jalan Kantor Polisi no 1'


### A.5 Aturan Penamaan Variable

- Gunakan huruf kecil
- Gunakan '_' untuk memisahkan kata
- Perhatikan penggunaan kata singular dan plural dalam english. Misalnya 'people'
  untuk variable array and 'person' untuk single human data.
- Don't be so cool, define human readable name

**Contoh:**

Bad:

	fname   = 'John'
	w1      = 40
	w2      = 50
	total   = w1 + w2
  	persons = Human.all
  	city    = ['Bandung', 'Tasikmalaya', 'Garut', 'Sumedang']

Good:

	first_name    = 'John'
	first_weight  = 0.1
	second_weight = 0.2
	total_weight  = first_weight + second_weight
  	people        = Human.all
  	cities        = ['Bandung', 'Tasikmalaya', 'Garut', 'Sumedang']


### A.6 Aturan Penamaan Method

- Gunakan huruf kecil
- Gunakan '_' untuk memisahkan kata
- Gunakan '?' di akhir nama method yang mengembalikan boolean
- Same as above, please use human readable name

**Contoh:**

Bad:

	def fullName
	  ...
	end

	def check_name
      first_name.present? || last_name.present?
	end

	def export_excell
  	  ...
	end

Good:

	def full_name
	  ...
	end

	def name_exist?
	  first_name.present? || last_name.present?
	end

	def export_people_name_to_excell
  	  ...
	end


## B Line & Words Spacing Convention

### B.1 Indentation
- Gunakan 2 spasi
- Jangan gunakan tab

**Contoh:**

	def full_name
	  if name_present?
		"#{first_name} #{last_name}"
      end
    end


### B.2 Assignments & Operations

- Lets write beautifull codes
- Buat codes assignment terlihat sejajar
- Kalau ada code yang terlalu panjang jika dibuat satu baris, maka buat menjadi beberapa baris

**Contoh:**

    first_name  = 'Feby'
    last_name   = 'Artandi'
    email       = 'feby@doterb.com'

    scope :for_user, lambda { |user_id|
      where(user_id: user_id)
    }

    scope :active_future, lambda {
      where('archived = false and (
        (start_date ISNULL AND end_date ISNULL) OR
        start_date >= current_date at time zone \'utc\' OR current_date at time zone \'utc\' < end_date
      )')
    }


## C. String Initialization & Operation

- Gunakan single quote kecuali jika ada interpolation
- Jangan gunakan (+) untuk membuat kombinasi 2 / lebih variable string tapi
  pakai interpolation

**Contoh**

Bad:

	first_name = "Paul"
	last_name  = "Gilbert"
	full_name  = first_name + last_name

Good:

	first_name = 'Paul'
	last_name  = 'Gilbert'
	full_name  = "#{first_name} #{last_name}"


## D. Conditional Statement

- Hindari nested conditional
- Tidak perlu 'return' untuk mengembalikan single value di akhir statement

**Contoh:**

Bad

	def full_name
	  result = nil
      if first_name.blank? && last_name.blank?
        result = "No Name"
      else
        result = "#{first_name} #{last_name}"
      end
      result
    end

Good

	def full_name
      return 'no name' if first_name.blank? && last_name.blank?
      "#{first_name} #{last_name}"
    end

 Bad

 	def profile_image
   	  if user.profile_image_url.present?
        return user.profile_image
   	  else
     	return 'default_image.jpg'
   	  end
 	end

 Good

	def profile_image
      profile_image_image_url || 'default_image.jpg'
    end


## E. Declaration Orders in Class

Dalam membuat codes di dalam class, seringkali kita menambahkan code di posisi yang asal
kita klik saja, sehingga urutan sering tidak konsisten. Ada yang deklarasi relasi paling atas,
diikuti dengan validasi, tapi di model lainnya kebalik. Untuk menghindari itu, kita tetapkan
urutan penambahan code di dalam class sebagai berikut:


1. require
2. include
3. relasi

        belongs_to
        has_one
        has_many

4. validasi
5. constant
6. scope
7. gem setup, e.g algolia setup, devise setup
8. class methods / self method
9. methods
10. protected methods
11. private methods


## F. Line of Codes Number

Satu method tidak boleh lebih dari 10 baris ( termasuk keyword if, end, while etc ), idealnya
adalah selalu di bawah 7 baris. Jika code Anda butuh lebih dari 10 baris, buat menjadi method terpisah.
Jika tidak menemukan pilihan, diskusi dengan team Anda dan buat alasan yang bisa diterima.


## G. Params numbers

Satu method maximum hanya boleh menerima 4 parameters. Cari solusi sebisa mungkin agar params tidak
melebihi 4. Jika tidak menemukan solusi untuk meminimalisasi params, maka gunakan hash.


## H. Keep DRY
- Selalu disiplin pada prinsip DRY ( Do not repeat yourself ) dengan tidak membuat method yang berfungsi
  sama persis. Jika Anda membutuhkan untuk membuat method baru, selalu cari dulu dari code yang sudah ada
  dengan 'grep' atau 'editor search' atau lihat manual, jangan asal buat baru.
- Jika Anda perlu method yang fungsinya sama digunakan dalam 2 class yang berbeda, maka gunakan concern
- Jika Anda perlu non instance method seperti fungsi perhitungan, export yang tidak embed kedalam class
  tapi kemungkinan akan digunakan di lebih dari satu class, maka buat modul di folder lib.

## I. Assets Management

### I.1 Javascript

- Jangan ada inline javascript kecuali untuk embed script seperti fb_login, google_script,
  intercom dan sebagainya.
- Penamaan file menggunakan huruf kecil
- Gunakan '-' untuk memisahkan kata
- Simpan file di app/assets/javascripts/controller_name/action_name.js

  **Contoh**

  Anda perlu menambahkan file javascript pada halaman products/new, maka
  tambahkan file nya di path berikut ini:

  		app/assets/javascripts/products/new.js

  Jika dalam collections products itu ada halaman lain seperti products/index misalnya,   
  maka struktur file js nya seperti ini:

  		app/assets/javascripts/products/new.js
        app/assets/javascripts/products/index.js

  Read more about javascript code styles in [javascript code style](https://bitbucket.org/c-aio/developer_guidance/src/master/javascript.md)

### I.2 CSS

- Jangan ada inline CSS
- Penamaan file menggunakan huruf kecil
- Gunakan '-' untuk memisahkan kata
- Gunakan SCSS
- Implementasikan nested CSS untuk menghindari conflict style
- Simpan file di app/assets/styles/controller_name/action_name.js

**Contoh**

  Anda perlu menambahkan file styles pada halaman products/new, maka
  tambahkan file nya di path berikut ini:

  		app/assets/styles/products/new.scss

  Jika dalam collections products itu ada halaman lain seperti products/index misalnya,   
  maka struktur file css nya seperti ini:

  		app/assets/styles/products/new.scss
        app/assets/styles/products/index.scss

  Bagaimana jika anda ingin combine css yang related dengan products sekaligus?

  		app/assets/styles/products/mixin.scss

        import 'new.scss'
        import 'index.scss'

### I.3 Images

- Penamaan file menggunakan huruf kecil
- Gunakan '-' untuk memisahkan kata
- Simpan images di app/assets/images/collection_name

Contoh

		app/assets/images/products/no-pic-small.jpg
        app/assets/images/products/no-pic-large.jpg
